import datetime
import requests as r
import matplotlib.pyplot as plt

from bs4 import BeautifulSoup
from collections import namedtuple
from graphic import survey


base_url = 'https://emkn.ru'
courses_url = 'https://emkn.ru/learning/courses/'
login_url = 'https://emkn.ru/login/'

Task = namedtuple('Task', 'begin deadline')


def login():
    with open('login_password', 'r') as fin:
        login, password = fin.readline().split('; ')

    client = r.session()
    client.get(login_url)

    if 'csrftoken' in client.cookies:
        csrftoken = client.cookies['csrftoken']
    else:
        csrftoken = client.cookies['csrf']

    data = {
            'username': login,
            'password': password,
            'next': courses_url,
            'csrfmiddlewaretoken': csrftoken
            }

    resp = client.post(login_url, data=data, headers=dict(Referer=login_url))
    courses_resp = client.get(courses_url, data=data)
    with open('courses.html', 'wb') as fout:
        fout.write(courses_resp.content)

    return client, data


def parse_courses(client, data):
    with open('courses.html', 'rb') as fin:
        contents = fin.read()

    soup = BeautifulSoup(contents, 'lxml')
    links = {
            a['href'].split('/')[-2][5:]: base_url + a['href']
            for a in soup.table.tbody.find_all('a', href=True)
            if len(a.parent.find_all('span')) > 0 and 'courses' in a['href']
            }

    tasks = {}
    for name, link in links.items():
        resp = client.get(link, data=data)
        if name == '2020': name = '2020-matan'
        tasks[name] = parse_tasks(client, data, name, resp.content)
    return tasks


def parse_tasks(client, data, name, contents, group_filter='None'):
    soup = BeautifulSoup(contents, 'lxml')

    is_homework = lambda x: all(text not in x for text in ['Тестик', 'онтрольн', 'Тест'])
    div_table = soup.find('div', id='course-assignments')
    if div_table == None: 
        return []
    links = {
            e.text: base_url + e['href'] for e in div_table.descendants 
            if e.name == 'a' and e.text not in {'.pdf', '.tex', '.zip'} and is_homework(e.text) and e['href'][-1] == '/'
            }
    # homeworks_without_link = [
    #         e.td.div.text.strip().split('\n')[0] + ' 2020' for e in div_table.descendants 
    #         if e.name == 'tr' and e.td and e.td.div and is_homework(e.td.div.text)
    #         ]
    
    tasks = []
    for title, link in links.items():
        if group_filter not in title:
            resp = client.get(link, data=data)
            task_soup = BeautifulSoup(resp.content, 'lxml')
            div_info = task_soup.find('div', id='o-sidebar')
            deadline = div_info.find_all('span')[1].text.strip()[:-6]
            begin = div_info.text.split('\n')[-2].strip()

            if deadline == 'сегодня':
                tasks.append(Task(str_to_datetime(begin), datetime.datetime.now()))
            elif deadline == 'завтра':
                tasks.append(Task(str_to_datetime(begin), datetime.datetime.now() + datetime.timedelta(days=1)))
            elif deadline == 'вчера':
                tasks.append(Task(str_to_datetime(begin), datetime.datetime.now() - datetime.timedelta(days=1)))
            else:
                tasks.append(Task(str_to_datetime(begin), str_to_datetime(deadline)))

    # for hw in homeworks_without_link:
    #     deadline = str_to_datetime(hw)
    #     begin = deadline - datetime.timedelta(7)
    #     tasks.append(Task(begin, deadline))
    
    return tasks


def str_to_datetime(string):
    months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
    for month_number, month_name in enumerate(months):
        string = string.replace(month_name, str(month_number+1))

    return datetime.datetime.strptime(string, '%d %m %Y')


def daterange(start_date, end_date):
    for n in range(int((end_date-start_date).days)):
        yield start_date + datetime.timedelta(n)


def coverage(tasks):
    result = []
    for task in tasks:
        result += list(daterange(task.begin, task.deadline))
    return list(map(lambda d: str(d).split()[0], result))


def generate_statistics(tasks, start_date, end_date):
    names = sorted(tasks.keys())
    dates = {
            date: [coverage(tasks[name]).count(str(date)) for name in names]
            for date in daterange(start_date, end_date)
            }
    return dates


if __name__ == '__main__':
    client, data = login()
    tasks = parse_courses(client, data)
    begin_date = datetime.date(2021, 2, 11)
    end_date = datetime.date(2021, 4, 30)
    results = generate_statistics(tasks, begin_date, end_date)

    survey(results, sorted(tasks.keys()))
    plt.show()
